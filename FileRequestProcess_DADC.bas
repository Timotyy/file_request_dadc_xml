Attribute VB_Name = "FileRequestProcess_DADC_XML"
Option Explicit
Public g_strCetaConnection As String
Public g_strTransferConnection As String
Public cnn As ADODB.Connection
Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Public g_strAdministratorEmailAddress As String
Public g_strFailComment As String

Const g_strSMTPServer = "jca-eset.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = ""
Const g_strFullUserName = "MX1 London Operations"

Private Declare Function GetComputerName Lib "kernel32" _
   Alias "GetComputerNameA" (ByVal lpBuffer As String, _
   nSize As Long) As Long
   
Public Const MAX_COMPUTERNAME_LENGTH As Long = 15&
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Declare Function GetDiskFreeSpaceEx Lib "kernel32.dll" Alias "GetDiskFreeSpaceExA" (ByVal lpDirectoryName As String, lpFreeBytesAvailableToCaller As Currency, lpTotalNumberOfBytes As Currency, lpTotalNumberofFreeBytes As Currency) As Long

Public Sub RunFileRequestProcess()

Dim FreeBytesAvailableToCaller As Currency, TotalNumberOfBytes As Currency, TotalNumberofFreeBytes As Currency, l_curSuccess As Currency

Dim l_rstRequests As ADODB.Recordset, l_lngClipID As Long, l_lngNewClipID As Long, l_lngLibraryID As Long, l_lngNewLibraryID As Long, l_strPathToFile As String, l_strNewPathToFile As String
Dim SQL As String, l_strCommandLine As String, l_strResult As String, l_lngFileHandle As Long, l_curFileSize As Currency
Dim l_datNow As Date, l_strCetaClientCode As String, l_rstExtras As ADODB.Recordset, l_strClipFormat As String
Dim FSO As Scripting.FileSystemObject, l_strFilename As String, l_strReference As String, l_strNewPath As String, l_strNewFileName As String, l_lngWriteCount As Long, l_lngRepeatcount As Long, l_lngCount As Long
Dim l_strComputerName As String, Sub_Path As String, Rebuilt_path As String, Email_Message As String, l_rstEmail As ADODB.Recordset
Dim myObject As Ceta_Checksum_Library.ChecksumClass
Dim Success As Boolean
Dim l_strNewFolder As String, l_strNewReference As String, l_strFileList As String
Dim l_lngJobID As Long, l_rstContacts As ADODB.Recordset, l_rstCCUsers As ADODB.Recordset, l_lngPortalUserID As Long, l_lngAsperaLibraryID As Long, l_strAsperaFolder As String
Dim l_strCCEmailList As String, l_strEmailBody As String
Dim l_rstClips As ADODB.Recordset, l_lngVerifyGoodFiles As Long, l_lngVerifyFailedFiles As Long, l_blnNotMovedFlag As Boolean, Message As String
Dim l_intCount As Long, l_intHours As Long
Dim l_strErrorDescription As String, Requestnumber As Long
Dim l_strChecksumType As String
Dim l_intUrgent As Long
Dim l_lngRunningJobCount As Long, l_lngMaxNumberOfRunningJobs As Long, l_lngCorrespondingRequestID As Long, l_lngJobsToStart As Long
Dim l_lngCompanyID As Long, l_strCETACompanySettings As String, l_lngAutoDeleteDays As Long, l_lngAutoDeleteLibraryID As Long, l_lngTempAutoDeleteLibraryID As Long, l_strAutoDeleteFolder As String
Dim LockFlag As Long
Dim l_blnDADC_VOD As Boolean, l_blnDADC_Send As Boolean, l_blnDADC_IsResend As Boolean, l_strRequestName As String

Set FSO = New Scripting.FileSystemObject

cnn.Open g_strCetaConnection

Set l_rstRequests = New ADODB.Recordset

LockFlag = GetData("setting", "value", "name", "LockSystem")

If (LockFlag And 4096) = 4096 Then
    cnn.Close
    Exit Sub
End If

SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
l_rstRequests.Open SQL, cnn, 3, 3
If l_rstRequests.RecordCount > 0 Then
    g_strUserEmailAddress = l_rstRequests("value")
End If
l_rstRequests.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
l_rstRequests.Open SQL, cnn, 3, 3
If l_rstRequests.RecordCount > 0 Then
    g_strUserEmailName = l_rstRequests("value")
End If
l_rstRequests.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
l_rstRequests.Open SQL, cnn, 3, 3
If l_rstRequests.RecordCount > 0 Then
    g_strAdministratorEmailAddress = l_rstRequests("value")
End If
l_rstRequests.Close

l_strComputerName = CurrentMachineName()
l_lngWriteCount = 0
l_lngRunningJobCount = 0

SQL = "SELECT TOP 1 * FROM Event_File_Request " & _
"WHERE event_file_request_TypeID IN (SELECT event_file_Request_TypeID FROM event_file_request_type WHERE description = 'DADC XML') " & _
"AND RequestComplete IS NULL and RequestFailed IS NULL AND RequestStarted IS NULL " & _
"ORDER BY CASE WHEN Urgent <> 0 THEN 0 ELSE 1 END, RequestDate;"

Debug.Print SQL

l_rstRequests.Open SQL, cnn, 3, 3
If l_rstRequests.RecordCount = 1 Then

    l_strPathToFile = ""
    l_strNewPathToFile = ""
    l_rstRequests.MoveFirst
    Requestnumber = l_rstRequests("Event_File_requestID")
    l_rstRequests.Close
    SetData "event_file_request", "RequestStarted", "event_file_requestID", Requestnumber, Format(Now, "YYYY-MM-DD HH:NN:SS")
    SetData "event_file_request", "Handler", "event_file_requestID", Requestnumber, l_strComputerName
    l_lngClipID = Val(Trim(" " & GetData("event_file_request", "eventID", "event_file_requestID", Requestnumber)))
    l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
    l_blnDADC_VOD = GetData("event_file_request", "DADC_VOD", "event_file_requestID", Requestnumber)
    l_blnDADC_Send = GetData("event_file_request", "DADC_Send_XML", "event_file_requestID", Requestnumber)
    l_blnDADC_IsResend = GetData("event_file_request", "DADC_Is_Resend", "event_file_requestID", Requestnumber)
    l_strRequestName = GetData("event_file_request", "RequestName", "event_file_requestID", Requestnumber)
    g_strFailComment = ""
    
    If MakeDADCXMLFile(l_lngClipID, l_lngLibraryID, l_blnDADC_VOD, l_blnDADC_Send, l_blnDADC_IsResend, l_strRequestName, Requestnumber) Then
        SetData "event_file_request", "RequestComplete", "event_file_requestID", Requestnumber, Format(Now, "YYYY-MM-DD HH:NN:SS")
        SetData "event_file_request", "Comment", "event_file_requestID", Requestnumber, "XML Made" & IIf(GetData("event_file_request", "DADC_Send_XML", "event_file_requestID", Requestnumber) <> 0, " and Sent", "")
    Else
        SetData "event_file_request", "RequestFailed", "event_file_requestID", Requestnumber, Format(Now, "YYYY-MM-DD HH:NN:SS")
        SetData "event_file_request", "Comment", "event_file_requestID", Requestnumber, g_strFailComment
    End If
Else
    l_rstRequests.Close
End If
    
Set l_rstRequests = Nothing

cnn.Close
Exit Sub

End Sub

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, Optional lp_ReturnTrueNulls As Boolean) As Variant

Dim l_strSQL As String

l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

Dim l_rstGetData As New ADODB.Recordset
Dim l_con As New ADODB.Connection

l_con.Open g_strCetaConnection
l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

If Not l_rstGetData.EOF Then

    If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
        GetData = l_rstGetData(lp_strFieldToReturn)
        GoTo Proc_CloseDB
    End If

Else
    GoTo Proc_CloseDB

End If

If lp_ReturnTrueNulls = True Then
    GetData = Null
Else
    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select
End If

Proc_CloseDB:

On Error Resume Next

l_rstGetData.Close
Set l_rstGetData = Nothing

l_con.Close
Set l_con = Nothing

Exit Function

End Function

Function GetDataSQL(lp_strSQL As String)

On Error GoTo Proc_GetData_Error

Dim l_rstGetData As New ADODB.Recordset
Dim l_conGetData As New ADODB.Connection

l_conGetData.Open g_strCetaConnection
l_rstGetData.Open lp_strSQL, l_conGetData, 3, 3

If Not l_rstGetData.EOF Then
    
    If Not IsNull(l_rstGetData(0)) Then
        GetDataSQL = l_rstGetData(0)
    Else
        Select Case l_rstGetData.Fields(0).Type
        Case adChar, adVarChar, adVarWChar, 201
            GetDataSQL = ""
        Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
            GetDataSQL = 0
        Case adDate, adDBDate, adDBTime, adDBTimeStamp
            GetDataSQL = 0
        Case Else
            GetDataSQL = False
        End Select
    End If

End If

l_rstGetData.Close
Set l_rstGetData = Nothing

l_conGetData.Close
Set l_conGetData = Nothing

Exit Function

Proc_GetData_Error:

GetDataSQL = ""

End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & QuoteSanitise(lp_varValue) & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

Set c = New ADODB.Connection
c.Open g_strCetaConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Public Function FormatSQLDate(lDate As Variant, Optional lp_blnMySQL As Boolean)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    If lp_blnMySQL = True Then
        FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Format(Hour(lDate), "00") & ":" & Format(Minute(lDate), "00") & ":" & Format(Second(lDate), "00")
    Else
        FormatSQLDate = Month(lDate) & "/" & Day(lDate) & "/" & Year(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetNextSequence(lp_strSequenceName As String) As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String, c As ADODB.Connection
    Dim l_lngGetNextSequence  As Long
    l_strSQL = "SELECT * FROM sequence WHERE sequencename = '" & lp_strSequenceName & "';"
    
    Set c = New ADODB.Connection
    c.Open g_strCetaConnection
    Dim l_rsSequence As ADODB.Recordset
    Set l_rsSequence = New ADODB.Recordset
    l_rsSequence.Open l_strSQL, c, 3, 3
    
    If Not l_rsSequence.EOF Then
        l_rsSequence.MoveFirst
        l_lngGetNextSequence = l_rsSequence("sequencevalue")
                
        l_strSQL = "UPDATE sequence SET sequencevalue = '" & l_lngGetNextSequence + 1 & "', muser = 'ASPERA', mdate = '" & FormatSQLDate(Now()) & "' WHERE sequencename = '" & lp_strSequenceName & "';"
        
        c.Execute l_strSQL
        
    Else
        
        Dim l_strSequenceNumber As String
PROC_Input_Number:
        l_strSequenceNumber = InputBox("There is currently no default value for a sequence of " & lp_strSequenceName & ". Please specify the initial value.", "No default value")
        If Not IsNumeric(l_strSequenceNumber) Then
            MsgBox "Please enter a valid numeric, positive integer.", vbExclamation
            GoTo PROC_Input_Number
        Else
            l_rsSequence.AddNew
            l_rsSequence("sequencename") = lp_strSequenceName
            l_rsSequence("sequencevalue") = Val(l_strSequenceNumber) + 1
            l_rsSequence("muser") = "ASPERA"
            l_rsSequence("mdate") = Now()
            l_rsSequence.Update
            l_lngGetNextSequence = Val(l_strSequenceNumber)
        End If
    End If
    
    l_rsSequence.Close
    Set l_rsSequence = Nothing
    
    c.Close
    Set c = Nothing
    GetNextSequence = l_lngGetNextSequence
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function CopyEventToLibraryID(lp_lngEventID As Long, lp_lngLibraryID As Long) As Long

'copy the info from one job to another
Dim l_rstOriginalEvent As ADODB.Recordset
Dim l_rstNewEvent As ADODB.Recordset, l_lngNewEventID As Long
Dim c As ADODB.Connection
Dim l_rstTemp As ADODB.Recordset, l_lngtechrevID As Long

'get the original job's details
Dim l_strSQL As String

Set c = New ADODB.Connection
c.Open g_strCetaConnection

l_strSQL = "INSERT INTO events ("
l_strSQL = l_strSQL & "eventmediatype, libraryID, companyID) VALUES ("
l_strSQL = l_strSQL & "'FILE', " & lp_lngLibraryID & ", 1);SELECT SCOPE_IDENTITY();"

Debug.Print l_strSQL
Set l_rstOriginalEvent = New ADODB.Recordset
Set l_rstOriginalEvent = c.Execute(l_strSQL)

l_lngNewEventID = l_rstOriginalEvent.NextRecordset().Fields(0).Value

l_strSQL = "SELECT * FROM events WHERE eventID = '" & lp_lngEventID & "';"
l_rstOriginalEvent.Open l_strSQL, c, adOpenDynamic, adLockBatchOptimistic

'allow adding of a new job
l_strSQL = "SELECT * FROM events WHERE eventID = '" & l_lngNewEventID & "';"
Set l_rstNewEvent = New ADODB.Recordset
l_rstNewEvent.Open l_strSQL, c, adOpenDynamic, adLockOptimistic

Dim l_intLoop As Integer
Dim l_lngConflict As Long
    
If l_rstOriginalEvent.EOF Then Exit Function

l_rstNewEvent("libraryID") = lp_lngLibraryID

For l_intLoop = 0 To l_rstOriginalEvent.Fields.Count - 1
    If l_rstOriginalEvent.Fields(l_intLoop).Name <> "libraryID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "eventID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "bigfilesize" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "MediaWindowDeleteDate" Then
    
        If Not IsNull(l_rstOriginalEvent(l_intLoop)) Then
            l_rstNewEvent(l_intLoop) = l_rstOriginalEvent(l_intLoop)
        End If
    
    End If
Next

l_rstNewEvent("mdate") = Null
l_rstNewEvent("muser") = Null

l_rstNewEvent.Update

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalEvent.Close

'close the new record
l_rstNewEvent.Close

'Copy the Segment Information re-using the l_rstOriginalEvent to read the records
l_rstOriginalEvent.Open "SELECT * FROM eventsegment WHERE eventID = " & lp_lngEventID, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO eventsegment (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("segmentreference")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestart") & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestop") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("note")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

'Copy the Logging Information re-using the l_rstOriginalEvent to read the records
l_rstOriginalEvent.Open "SELECT * FROM eventlogging WHERE eventID = " & lp_lngEventID, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("segmentreference")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestart") & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestop") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("note")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

'Copy the iTunes Advisory Information
l_strSQL = "SELECT * FROM iTunes_Clip_Advisory WHERE eventID = " & lp_lngEventID & ";"
l_rstOriginalEvent.Open l_strSQL, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO iTunes_Clip_Advisory (eventID, advisorysystem, advisory) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("advisorysystem")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("advisory")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

'Copy the iTunes Product Details
l_strSQL = "SELECT * FROM iTunes_product WHERE eventID = " & lp_lngEventID & ";"
l_rstOriginalEvent.Open l_strSQL, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO iTunes_product (eventID, territory, salesstartdate, clearedforsale) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("territory")) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_rstOriginalEvent("salesstartdate")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("clearedforsale")) & "');"
        Debug.Print l_strSQL
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

Set l_rstOriginalEvent = Nothing
Set l_rstNewEvent = Nothing

CopyEventToLibraryID = l_lngNewEventID

End Function

Public Function CurrentMachineName() As String
            
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim lSize As Long
    Dim sBuffer As String
    sBuffer = Space$(MAX_COMPUTERNAME_LENGTH + 1)
    lSize = Len(sBuffer)
    
    If GetComputerName(sBuffer, lSize) Then
        CurrentMachineName = Left$(sBuffer, lSize)
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function CheckLocal10Gig() As Boolean

Dim IPConfig As Variant
Dim IPConfigSet As Object

Set IPConfigSet = GetObject("winmgmts:{impersonationLevel=impersonate}").ExecQuery("SELECT IPAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = TRUE")

CheckLocal10Gig = False

For Each IPConfig In IPConfigSet
    If Not IsNull(IPConfig.ipaddress) Then
        If InStr(Trim(" " & IPConfig.ipaddress(0)), "10.9.7.") > 0 Or InStr(Trim(" " & IPConfig.ipaddress(0)), "10.9.77.") > 0 Then
            CheckLocal10Gig = True
        End If
    End If
Next IPConfig

End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strCetaConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Function FTPUpload(lp_strRemoteFilename As String, lp_strLocalFile As String, Optional lp_strRemoteDirectory As String) As Boolean

Dim Success As Integer, Count As Integer
Dim ftp As New ChilkatFtp2

' Any string unlocks the component for the 1st 30-days.
Success = ftp.UnlockComponent("JCATVrFTP_Wbz2YPz5lzup")
If (Success <> 1) Then
    FTPUpload = False
    Exit Function
End If

ftp.HostName = GetData("setting", "value", "name", "MX1SmartjogFTPhostname")
ftp.Username = GetData("setting", "value", "name", "MX1SmartjogFTPusername")
ftp.Password = GetData("setting", "value", "name", "MX1SmartjogFTPpassword")

ftp.Passive = 1

' The default data transfer mode is "Active" as opposed to "Passive".

' Connect and login to the FTP server.
Success = 0: Count = 0
While Success <> 1 And Count < 10
    Success = ftp.Connect()
    If (Success <> 1) Then
        FTPUpload = False
        Count = Count + 1
    End If
Wend
If Success <> 1 Then Exit Function

' Change to the remote directory where the file is located.
' This step is only necessary if the file is not in the root directory
' for the FTP account.
If Not IsEmpty(lp_strRemoteDirectory) And lp_strRemoteDirectory <> "/" Then
    Success = ftp.ChangeRemoteDir(lp_strRemoteDirectory)
    If (Success <> 1) Then
        FTPUpload = False
        Exit Function
    End If
End If

' Upload a file.
Success = ftp.PutFile(lp_strLocalFile, lp_strRemoteFilename)
If (Success <> 1) Then
    FTPUpload = False
    Exit Function
End If

ftp.Disconnect
FTPUpload = True

End Function

Function VerifyFile(lp_lngEventID As Long, lp_lngLibraryID As Long) As Boolean

Dim l_strPathToFile As String, l_strFilename As String, l_curFileSize As Currency, l_lngFileHandle As Long, FSO As Scripting.FileSystemObject

If CheckLocal10Gig = True Then
    l_strPathToFile = GetData("library", "foreignref", "libraryID", lp_lngLibraryID)
    If l_strPathToFile = "" Then l_strPathToFile = GetData("library", "subtitle", "libraryID", lp_lngLibraryID)
Else
    If l_strPathToFile = "" Then l_strPathToFile = GetData("library", "subtitle", "libraryID", lp_lngLibraryID)
End If
l_strFilename = GetData("events", "clipfilename", "eventID", lp_lngEventID)
If Trim(" " & GetData("events", "altlocation", "eventID", lp_lngEventID)) <> "" Then l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", lp_lngEventID)
l_strPathToFile = l_strPathToFile & "\" & l_strFilename

On Error GoTo VERIFYFAILED

Set FSO = New Scripting.FileSystemObject

If FSO.FileExists(l_strPathToFile) Then
    API_OpenFile l_strPathToFile, l_lngFileHandle, l_curFileSize
    API_CloseFile l_lngFileHandle
    SetData "events", "bigfilesize", "eventID", lp_lngEventID, l_curFileSize
    SetData "events", "soundlay", "eventID", lp_lngEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
    VerifyFile = True
ElseIf FSO.FolderExists(l_strPathToFile) Then
    l_curFileSize = GetDirectorySize(l_strPathToFile)
    SetData "events", "bigfilesize", "eventID", lp_lngEventID, l_curFileSize
    SetData "events", "soundlay", "eventID", lp_lngEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
    SetData "events", "clipformat", "eventID", lp_lngEventID, "Folder"
    VerifyFile = True
Else
    SetData "events", "bigfilesize", "eventID", lp_lngEventID, "Null"
    SetData "events", "soundlay", "eventID", lp_lngEventID, "Null"
    VerifyFile = False
End If

END_FUNCTION:

Exit Function

VERIFYFAILED:
VerifyFile = False
Resume END_FUNCTION

End Function

Function GetCETASetting(lp_strSettingName As String) As Variant

GetCETASetting = GetData("setting", "value", "name", lp_strSettingName)

End Function

Function GetCount(lp_strSQL As String) As Double

Screen.MousePointer = vbHourglass

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strCetaConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open lp_strSQL, l_conSearch, adOpenStatic, adLockReadOnly
End With

l_rstSearch.ActiveConnection = Nothing

If l_rstSearch.EOF Then
    GetCount = 0
Else

    If Not IsNull(l_rstSearch(0)) Then
        GetCount = Val(l_rstSearch(0))
    Else
        GetCount = 0
    End If
End If

l_rstSearch.Close
Set l_rstSearch = Nothing

l_conSearch.Close
Set l_conSearch = Nothing

Screen.MousePointer = vbDefault

End Function

Function MakeDADCXMLFile(lp_lngEventID As Long, lp_libraryID As Long, lp_blnVOD As Boolean, lp_blnDADC_Send As Boolean, lp_blnDADC_Is_Resend As Boolean, RequestName As String, Requestnumber As Long) As Boolean

Dim l_strFileNameToSave As String, l_lngChannelCount As Long, l_strProgramStart As String, l_strProgramEnd As String, l_blnFoundProg As Boolean, l_blnFoundProgEnd As Boolean, l_strDescriptionAlias As String
Dim FSO As Scripting.FileSystemObject, l_strSQL As String
Dim l_strPathForMove As String, l_strFolderForFilerequest As String, l_strPathForCopy As String
Dim l_lngNewLibraryID As Long
Dim l_rsEmailToSend As ADODB.Recordset
Dim l_blnResend As Boolean
Dim l_lngXMLclipID As Long
Dim ChannelCount As Long
Dim txtReference As String, cmbClipCodec As String, txtTimecodestart As String, txtTimecodestop As String, cmbFrameRate As String, cmbIsTextInPicture As String, txtDuration As String, adoLogging As ADODB.Recordset
Dim lblAudioConfiguration As String, txtClipfilename As String, txtVertPixels As String, txtHorizpixels As String, cmbAspect As String, cmbInterlace As String, txtColorSpace As String, cmbClipformat As String
Dim cmbAudioType(8) As String, cmbAudioLanguage(8) As String, cmbAudioContent(8) As String
Dim Framerate As Integer, BigFileSize As Currency
Dim l_lngDADC_Archive_ID As Long
Dim cnnTransfer As ADODB.Connection
Dim l_lngDeliveryRequestID As Long
Dim l_lngDeliveryLibraryID As Long

l_lngDADC_Archive_ID = GetData("events", "libraryID", "eventID", lp_lngEventID)
l_lngDeliveryLibraryID = GetData("setting", "intvalue", "name", "DADC_Store_DeliveryLibraryID")

txtReference = GetData("events", "clipreference", "eventID", lp_lngEventID)
txtClipfilename = GetData("events", "clipfilename", "eventID", lp_lngEventID)
cmbClipCodec = GetData("events", "clipcodec", "eventID", lp_lngEventID)
txtTimecodestart = GetData("events", "Timecodestart", "eventID", lp_lngEventID)
txtTimecodestop = GetData("events", "Timecodestop", "eventID", lp_lngEventID)
cmbFrameRate = GetData("events", "clipFrameRate", "eventID", lp_lngEventID)
cmbIsTextInPicture = GetData("events", "IsTextInPicture", "eventID", lp_lngEventID)
txtDuration = GetData("events", "fd_length", "eventID", lp_lngEventID)
lblAudioConfiguration = Replace(Trim(" " & GetDataSQL("SELECT TOP 1 audioconfiguration FROM tracker_dadc_item WHERE itemreference = '" & txtReference & "' AND readytobill = 0;")), "&", "&&")
cmbAspect = GetData("events", "aspectratio", "eventID", lp_lngEventID)
txtVertPixels = GetData("events", "clipverticalpixels", "eventID", lp_lngEventID)
txtHorizpixels = GetData("events", "cliphorizontalpixels", "eventID", lp_lngEventID)
BigFileSize = GetData("events", "bigfilesize", "eventID", lp_lngEventID)
cmbInterlace = GetData("events", "interlace", "eventID", lp_lngEventID)
txtColorSpace = GetData("events", "ColorSpace", "eventID", lp_lngEventID)
cmbClipformat = GetData("events", "clipformat", "eventID", lp_lngEventID)

For ChannelCount = 1 To 8
    cmbAudioType(ChannelCount) = GetData("events", "audiotypegroup" & ChannelCount, "eventID", lp_lngEventID)
    cmbAudioLanguage(ChannelCount) = GetData("events", "audiolanguagegroup" & ChannelCount, "eventID", lp_lngEventID)
    cmbAudioContent(ChannelCount) = GetData("events", "audiocontentgroup" & ChannelCount, "eventID", lp_lngEventID)
Next

Select Case cmbFrameRate
    Case "25"
        Framerate = TC_25
    Case "29.97"
        Framerate = TC_29
    Case "30", "29.97 NDF"
        Framerate = TC_30
    Case "24", "23.98"
        Framerate = TC_24
    Case Else
        Framerate = TC_UN
End Select

If Val(Trim(" " & GetDataSQL("SELECT TOP 1 stagefield4 FROM tracker_dadc_item WHERE itemreference = '" & txtReference & "' AND stagefield15 IS NULL"))) <> 0 Then
    g_strFailComment = "You have already made the XML for this tracker item, and it is either in the queue to send or has already been sent."
    Exit Function
End If

If cmbClipCodec <> "ProRes HQ" And cmbClipCodec <> "ProRes HQ UPRES" And cmbClipCodec <> "ProRes 4444" And cmbClipCodec <> "ProRes 4444 UPRES" Then
    g_strFailComment = "File is not a ProRes masterfile - you cannot send this."
    Exit Function
End If

l_blnFoundProg = False
Set adoLogging = New ADODB.Recordset
adoLogging.Open "SELECT * FROM eventLogging WHERE eventID = " & lp_lngEventID, cnn, 3, 3

adoLogging.MoveFirst
Do While Not adoLogging.EOF
    If adoLogging("segmentreference") = "Programme" Then
        l_blnFoundProg = True
        Exit Do
    End If
    adoLogging.MoveNext
Loop

If l_blnFoundProg = False Then
    g_strFailComment = "There is no 'Programme' item logged - you cannot send this."
    Exit Function
End If

Select Case Val(lp_libraryID)
    Case 770131, 770132, 770133, 770134, 770135, 770041
        'Do nothing
    Case Else
        MsgBox "Masterfile is not on the the Correct Store. You cannot send this.", vbCritical, "Not on " & GetData("library", "barcode", "libraryID", l_lngDADC_Archive_ID)
        Exit Function
End Select

On Error GoTo DADC_XML_ERROR3

If txtReference <> "" And txtTimecodestart <> "" And txtTimecodestop <> "" And cmbFrameRate <> "" And cmbIsTextInPicture <> "" And Len(txtDuration) = 11 _
And ((lp_blnVOD = False And adoLogging.RecordCount >= 4) Or (lp_blnVOD = True And adoLogging.RecordCount >= 2)) Then
    
    If GetData("events", "libraryID", "eventID", lp_lngEventID) = 770041 Then
        l_strFileNameToSave = GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", lp_lngEventID)) & "\ceta\Ceta\1261\XML_Sent\" & txtReference & ".xml"
    Else
        l_strFileNameToSave = GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", lp_lngEventID)) & "\1261\XML_Sent\" & txtReference & ".xml"
    End If
    
    
    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""utf-8""?>"
    Print #1, "<BulkIngestData xsi:schemaLocation=""http://schema.dadcdigital.com/dbb/data/2010/migration/"" xmlns=""http://schema.dadcdigital.com/dbb/data/2010/migration/"" xmlns:inv=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"" xmlns:z=""http://schemas.microsoft.com/2003/10/Serialization/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">"
    WriteTabs 1
    Print #1, "<ExternalTaskID></ExternalTaskID>"
    WriteTabs 1
    Print #1, "<ExternalPONumber></ExternalPONumber>"
    WriteTabs 1
    Print #1, "<ExternalDescription></ExternalDescription>"
    WriteTabs 1
    If Trim(" " & GetData("tracker_dadc_item", "externalalphaID", "itemreference", txtReference)) = "" Then
        Print #1, "<ExternalAlphaID>" & GetData("tracker_dadc_item", "contentversioncode", "itemreference", txtReference) & "</ExternalAlphaID>"
    Else
        Print #1, "<ExternalAlphaID>" & GetData("tracker_dadc_item", "externalalphaID", "itemreference", txtReference) & "</ExternalAlphaID>"
    End If
    WriteTabs 1
    
    'File Components - a Video section
    Print #1, "<Components>"
    WriteTabs 2
    Print #1, "<Component xsi:type=""inv:VideoComponent"">"
    WriteTabs 3
    Print #1, "<inv:TapeBarcode xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & GetData("tracker_dadc_item", "barcode", "itemreference", txtReference) & "</inv:TapeBarcode>"
    If GetData("tracker_dadc_item", "UID", "itemreference", txtReference) <> "" Then
        WriteTabs 3
        Print #1, "<inv:ClockUID>" & GetData("tracker_dadc_item", "UID", "itemreference", txtReference) & "</inv:ClockUID>"
    End If

    WriteFileDescriptor lp_blnVOD, 3, "", lblAudioConfiguration, txtClipfilename, txtReference, txtVertPixels, cmbAspect, cmbFrameRate, cmbInterlace, txtColorSpace, cmbClipformat, cmbClipCodec
    
    WriteTabs 3
    Print #1, "<inv:VideoStandard xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/""></inv:VideoStandard>"
    WriteTabs 3
    Print #1, "<inv:ScreenAspectRatio xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
    If cmbAspect = "16:9" Then
        Print #1, "16x9";
    Else
        Print #1, "4x3";
    End If
    Print #1, "</inv:ScreenAspectRatio>"
    WriteTabs 3
    Print #1, "<inv:ActivePictureAspectRatio xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
    If GetData("tracker_dadc_item", "imageaspectratio", "itemreference", txtReference) <> "" Then
        Print #1, GetData("tracker_dadc_item", "imageaspectratio", "itemreference", txtReference);
    Else
        If cmbAspect = "16:9" Then
            Print #1, "1.78";
        Else
            Print #1, "1.33";
        End If
    End If
    Print #1, "</inv:ActivePictureAspectRatio>"
    WriteTabs 3
    Print #1, "<inv:IsTextInPicture xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
    Print #1, cmbIsTextInPicture;
    Print #1, "</inv:IsTextInPicture>"
    WriteTabs 3
    Print #1, "<inv:TextedLanguage xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
    If GetData("tracker_dadc_item", "language", "itemreference", txtReference) = "German" Then
        Print #1, "German (Germany)";
    Else
        Print #1, GetData("tracker_dadc_item", "language", "itemreference", txtReference);
    End If
    Print #1, "</inv:TextedLanguage>"
    WriteTabs 3
    Print #1, "<inv:BurnedInSubtitleLanguage xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
    If GetData("tracker_dadc_item", "subtitleslanguage", "itemreference", txtReference) = "No" Then
        Print #1, "None";
    Else
        Print #1, GetData("tracker_dadc_item", "subtitleslanguage", "itemreference", txtReference);
    End If
    Print #1, "</inv:BurnedInSubtitleLanguage>"
    WriteTabs 3
    Print #1, "<inv:Runtime xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & txtDuration & "</inv:Runtime>"
    WriteTabs 3
    Print #1, "<inv:ActiveTopLeftX xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">0</inv:ActiveTopLeftX>"
    WriteTabs 3
    Print #1, "<inv:ActiveTopLeftY xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">0</inv:ActiveTopLeftY>"
    WriteTabs 3
    Print #1, "<inv:ActiveBottomRightX xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & Val(txtHorizpixels) - 1 & "</inv:ActiveBottomRightX>"
    WriteTabs 3
    Print #1, "<inv:ActiveBottomRightY xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & Val(txtVertPixels) - 1 & "</inv:ActiveBottomRightY>"
    WriteTabs 2
    Print #1, "</Component>"
    
    'Then an Audio section for each Audio set logged group
    l_lngChannelCount = 0
    
    For ChannelCount = 1 To 8
    
        If cmbAudioType(ChannelCount) <> "" Then
            
            If cmbAudioType(ChannelCount) = "Standard Stereo" Or cmbAudioType(ChannelCount) = "Stereo" Or cmbAudioType(ChannelCount) = "LT/RT" Or cmbAudioType(ChannelCount) = "VOD Standard Stereo" Or cmbAudioType(ChannelCount) = "VOD LT/RT" Or Left(cmbAudioType(ChannelCount), 7) = "Dolby E" Or cmbAudioType(ChannelCount) = "Mono 2.0" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType(ChannelCount) & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent(ChannelCount)) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage(ChannelCount) & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "2track", l_lngChannelCount, lp_blnVOD, cmbAudioContent(ChannelCount), cmbAudioType(ChannelCount), txtClipfilename, txtReference, txtVertPixels
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            ElseIf cmbAudioType(ChannelCount) = "5.1" Or cmbAudioType(ChannelCount) = "VOD 5.1" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType(ChannelCount) & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent(ChannelCount)) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage(ChannelCount) & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "6track", l_lngChannelCount, lp_blnVOD, cmbAudioContent(ChannelCount), cmbAudioType(ChannelCount), txtClipfilename, txtReference, txtVertPixels
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            ElseIf cmbAudioType(ChannelCount) = "Mono" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType(ChannelCount) & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent(ChannelCount)) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage(ChannelCount) & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "1track", l_lngChannelCount, lp_blnVOD, cmbAudioContent(ChannelCount), cmbAudioType(ChannelCount), txtClipfilename, txtReference, txtVertPixels
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            Else
            
                g_strFailComment = "Not equipped to do anything but Stereo, 5.1, Mono or Dolby E yet"
                Exit Function
            
            End If
        
        End If
    
    Next
        
    WriteTabs 1
    Print #1, "</Components>"
    
    'Then the Timecode section
    WriteTabs 1
    Print #1, "<TimecodeGroups>"
    WriteTabs 2
    Print #1, "<TimecodeGroup>"
    WriteTabs 3
    Print #1, "<TimecodeGroupType xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">Master Timecode Group</TimecodeGroupType>"
    WriteTabs 3
    Print #1, "<TimecodeRanges xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
    
    adoLogging.MoveFirst
    
    l_strProgramStart = ""
    l_strProgramEnd = ""
    l_blnFoundProg = False
    l_blnFoundProgEnd = False
    
    Do While Not adoLogging.EOF
    
        If Not (Validate_Timecode(adoLogging("timecodestart"), Framerate, True)) Or Not (Validate_Timecode(adoLogging("timecodestop"), Framerate, True)) Then
            
            g_strFailComment = "Problem with timecode logging on '" & adoLogging("segmentreference") & "'"
            Exit Function
        
        End If
        adoLogging.MoveNext
    
    Loop

    adoLogging.MoveFirst
    Do While Not adoLogging.EOF
        If adoLogging("segmentreference") = "Programme" Then
            
            If l_blnFoundProg = False Then
                l_strProgramStart = adoLogging("timecodestart")
                l_blnFoundProg = True
                Exit Do
            End If
        
        End If
        adoLogging.MoveNext
    Loop
    adoLogging.MoveLast
    Do While Not adoLogging.BOF
    
        If adoLogging("segmentreference") = "Programme" Then
            
            If l_blnFoundProg = True And l_blnFoundProgEnd = False Then
                If adoLogging("segmentreference") <> "Commercial_Blacks" Then
                    l_blnFoundProgEnd = True
                    l_strProgramEnd = adoLogging("timecodestop")
                    Exit Do
                End If
            End If

        End If
        adoLogging.MovePrevious
    Loop
    adoLogging.MoveFirst
                
    Do While Not adoLogging.EOF
    
        WriteTabs 4
        Print #1, "<TimecodeRange>"
        WriteTabs 5
        l_strDescriptionAlias = GetDataSQL("SELECT TOP 1 descriptionalias FROM xref WHERE category = 'dadclogging' and description = '" & adoLogging("segmentreference") & "';")
        If l_strDescriptionAlias <> "" Then
            Print #1, "<TimecodeRangeType>" & UCase(l_strDescriptionAlias) & "</TimecodeRangeType>"
        Else
            g_strFailComment = "The Logging item " & adoLogging("segmentreference") & " is not an approved item."
            GoTo ABORT_XML
        End If
        WriteTabs 5
        Print #1, "<InPointTimecode>" & adoLogging("timecodestart") & "</InPointTimecode>"
        WriteTabs 5
        Print #1, "<OutPointTimecode>" & FrameBefore(adoLogging("timecodestop"), Framerate) & "</OutPointTimecode>"
        If Trim(" " & adoLogging("note")) <> "" Then
            WriteTabs 5
            Print #1, "<TimecodeRangeDescription>" & adoLogging("note") & "</TimecodeRangeDescription>"
        End If
        WriteTabs 4
        Print #1, "</TimecodeRange>"
    
        adoLogging.MoveNext
    
    Loop
    
    If l_blnFoundProg = True And l_blnFoundProgEnd = True Then
        
        WriteTabs 4
        Print #1, "<TimecodeRange>"
        WriteTabs 5
        Print #1, "<TimecodeRangeType>PROGRAM_START</TimecodeRangeType>"
        WriteTabs 5
        Print #1, "<InPointTimecode>" & l_strProgramStart & "</InPointTimecode>"
        WriteTabs 5
        Print #1, "<OutPointTimecode>" & l_strProgramStart & "</OutPointTimecode>"
        WriteTabs 4
        Print #1, "</TimecodeRange>"
        WriteTabs 4
        Print #1, "<TimecodeRange>"
        WriteTabs 5
        Print #1, "<TimecodeRangeType>PROGRAM_END</TimecodeRangeType>"
        WriteTabs 5
        Print #1, "<InPointTimecode>" & FrameBefore(l_strProgramEnd, Framerate) & "</InPointTimecode>"
        WriteTabs 5
        Print #1, "<OutPointTimecode>" & FrameBefore(l_strProgramEnd, Framerate) & "</OutPointTimecode>"
        WriteTabs 4
        Print #1, "</TimecodeRange>"
    End If
                    
    WriteTabs 3
    Print #1, "</TimecodeRanges>"
    WriteTabs 2
    Print #1, "</TimecodeGroup>"
    WriteTabs 1
    Print #1, "</TimecodeGroups>"
    
    'Then close it all off
    Print #1, "</BulkIngestData>"

    Close #1
    
    adoLogging.Close
    Set adoLogging = Nothing

    l_strSQL = "UPDATE tracker_dadc_item SET stagefield3 = '" & FormatSQLDate(Now) & "' WHERE itemreference = '" & txtReference & "' AND readytobill = 0;"
    cnn.Execute l_strSQL

    'Potentially?, if you get to this point, you could then go ahead and 'Send' the files to DADC BBC DBB.
    'That would involve finding out whether it was a 'Send' or a 'Re-send', then copying this bunch of files in to the BBC Hot folder
    'Then also 'Moving' the files to the 'Sent' Archive folder
    
    If GetData("event_file_request", "DADC_Send_XML", "event_file_requestID", Requestnumber) <> 0 Then
        On Error GoTo SENDING_ERROR
        Set FSO = New Scripting.FileSystemObject
        l_strFolderForFilerequest = Format(Now, "YYYY_MM_DD")
        If GetData("event_file_request", "DADC_Is_Resend", "event_file_requestID", Requestnumber) <> 0 Then
            l_blnResend = True
            Set l_rsEmailToSend = New ADODB.Recordset
            l_rsEmailToSend.Open "SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 57", cnn, 3, 3
            If l_rsEmailToSend.RecordCount > 0 Then
                l_rsEmailToSend.MoveFirst
                Do While Not l_rsEmailToSend.EOF
                    SendSMTPMail l_rsEmailToSend("email"), l_rsEmailToSend("fullname"), "DADC Resend Initiated", "Please note the following file has been sent to a resend folder:" & vbCrLf & "CV Code: " & _
                    GetData("tracker_dadc_item", "contentversioncode", "itemreference", txtReference) & vbCrLf & "Filename: " & txtClipfilename & vbCrLf & _
                    "Item Folder: " & l_strFolderForFilerequest & vbCrLf, "", False, "", "", "thart@visualdatamedia.com", False, g_strUserEmailAddress
                    l_rsEmailToSend.MoveNext
                Loop
            End If
            l_rsEmailToSend.Close
        Else
            l_blnResend = False
        End If
        
        If l_blnResend = False Then
            If GetCETASetting("UseDADCFlipFlop") <> "0" Then
                If Val(GetCETASetting("BBC_Hotfolder_Flip")) = 1 Then
                    l_strPathForCopy = "Aspera-Push\DADC\DBB\BBCStudios1"
                    l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "DMZ-HOTFOLDERS")
                    cnn.Execute "UPDATE setting SET value = '2' WHERE name = 'BBC_Hotfolder_Flip'"
                Else
                    l_strPathForCopy = "Aspera-Push\DADC\DBB\BBCStudios2"
                    l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "DMZ-HOTFOLDERS")
                    cnn.Execute "UPDATE settiong SET vale = '1' WHERE name = 'BBC_Hotfolder_Flip'"
                End If
            Else
                l_strPathForCopy = "Aspera-Push\DADC\DBB\BBCStudios1"
                l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "DMZ-HOTFOLDERS")
            End If
        Else
            l_strPathForCopy = "Aspera-Push\DADC\DBB\BBCStudiosResend"
            l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "DMZ-HOTFOLDERS")
        End If
        'Copy to Hot Folder - send the xml first - get the xml into CETA, so we can put in a copy request and a move request for it.
        l_lngXMLclipID = CopyEventToLibraryID(lp_lngEventID, l_lngDADC_Archive_ID)
        If GetData("events", "libraryID", "eventID", lp_lngEventID) = 770041 Then
            SetData "events", "altlocation", "eventID", l_lngXMLclipID, "ceta\CETA\1261\XML_Sent"
        Else
            SetData "events", "altlocation", "eventID", l_lngXMLclipID, "1261\XML_Sent"
        End If
        SetData "events", "clipfilename", "eventID", l_lngXMLclipID, txtReference & ".xml"
        SetData "events", "clipformat", "eventID", l_lngXMLclipID, "XML File"
        If VerifyFile(l_lngXMLclipID, l_lngDADC_Archive_ID) = False Then
            g_strFailComment = "Sending Failed - Problem locating and copying XML file"
            Exit Function
        End If
        
        'If we are using DM then the order of things is...
        
        '0) Move both files into the Production Delivery Location, before we start delivering them.
        If Not FSO.FolderExists(GetData("library", "subtitle", "libraryID", l_lngDeliveryLibraryID) & "\" & _
        "production\ceta\deliveries\1261\AutoDelete30\" & l_strFolderForFilerequest) Then FSO.CreateFolder (GetData("library", "subtitle", "libraryID", l_lngDeliveryLibraryID) & "\" & _
        "production\ceta\deliveries\1261\AutoDelete30\" & l_strFolderForFilerequest)
        If Not FSO.FileExists(GetData("library", "subtitle", "libraryID", l_lngDADC_Archive_ID) & "\" & GetData("events", "altlocation", "eventID", l_lngXMLclipID) & "\" & _
        GetData("events", "clipfilename", "eventID", l_lngXMLclipID)) Then
            g_strFailComment = "Sending Failed - Problem locating and copying XML file"
            Exit Function
        End If
        FSO.CopyFile GetData("library", "subtitle", "libraryID", l_lngDADC_Archive_ID) & "\" & GetData("events", "altlocation", "eventID", l_lngXMLclipID) & "\" & _
        GetData("events", "clipfilename", "eventID", l_lngXMLclipID), GetData("library", "subtitle", "libraryID", l_lngDeliveryLibraryID) & "\" & _
        "production\ceta\deliveries\1261\AutoDelete30\" & l_strFolderForFilerequest & "\" & GetData("events", "clipfilename", "eventID", l_lngXMLclipID)
        FSO.DeleteFile GetData("library", "subtitle", "libraryID", l_lngDADC_Archive_ID) & "\" & GetData("events", "altlocation", "eventID", l_lngXMLclipID) & "\" & _
        GetData("events", "clipfilename", "eventID", l_lngXMLclipID)
        SetData "events", "altlocation", "eventID", l_lngXMLclipID, "production\ceta\deliveries\1261\AutoDelete30\" & l_strFolderForFilerequest
        SetData "events", "libraryID", "eventID", l_lngXMLclipID, l_lngDeliveryLibraryID
        If VerifyFile(l_lngXMLclipID, l_lngDeliveryLibraryID) = False Then
            g_strFailComment = "Sending Failed - Problem locating and copying XML file"
            Exit Function
        End If
        
        If Not FSO.FolderExists(GetData("library", "subtitle", "libraryID", l_lngDeliveryLibraryID) & "\" & _
        "production\ceta\deliveries\1261\AutoDelete30\" & l_strFolderForFilerequest) Then FSO.CreateFolder (GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", l_lngDeliveryLibraryID)) & "\" & _
        "production\ceta\deliveries\1261\AutoDelete30\" & l_strFolderForFilerequest)
        On Error GoTo MOVE_ERROR
        FSO.MoveFile GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", lp_lngEventID)) & "\" & GetData("events", "altlocation", "eventID", lp_lngEventID) & "\" & _
        GetData("events", "clipfilename", "eventID", lp_lngEventID), GetData("library", "subtitle", "libraryID", l_lngDeliveryLibraryID) & "\" & _
        "production\ceta\deliveries\1261\AutoDelete30\" & l_strFolderForFilerequest & "\" & GetData("events", "clipfilename", "eventID", lp_lngEventID)
        SetData "events", "altlocation", "eventID", lp_lngEventID, "production\ceta\deliveries\1261\AutoDelete30\" & l_strFolderForFilerequest
        SetData "events", "libraryID", "eventID", lp_lngEventID, l_lngDeliveryLibraryID
        If VerifyFile(lp_lngEventID, l_lngDeliveryLibraryID) = False Then
            g_strFailComment = "Sending Failed - Problem locating and copying asset file"
            Exit Function
        End If
        
        On Error GoTo SENDING_ERROR
        
        '1) Make a Request with StatusID 0
        g_strTransferConnection = GetData("setting", "value", "Name", "DADC_Transfer_Connection")
        Set cnnTransfer = New ADODB.Connection
        cnnTransfer.ConnectionString = g_strTransferConnection
        cnnTransfer.Open
        l_strSQL = "INSERT INTO Request (cdate, DueDate, PackageTitle, PreserveFolders, DestinationName, StatusID, KillMe, ServiceLevel) VALUES ("
        If l_blnResend = False Then
            l_strSQL = l_strSQL & "getdate(), getdate(), '" & txtReference & "', 0, '" & GetData("setting", "value", "name", "DADC_DDS_SendName") & "', 0, 0, 'CETA');SELECT SCOPE_IDENTITY();"
        Else
            l_strSQL = l_strSQL & "getdate(), getdate(), '" & txtReference & "', 0, '" & GetData("setting", "value", "name", "DADC_DDS_ReSendName") & "', 0, 0, 'CETA');SELECT SCOPE_IDENTITY();"
        End If
        Debug.Print l_strSQL
        Set l_rsEmailToSend = cnnTransfer.Execute(l_strSQL)
        l_lngDeliveryRequestID = l_rsEmailToSend.NextRecordset().Fields(0).Value
        l_strSQL = "INSERT INTO event_file_request (event_file_request_typeID, bigfilesize, RequestDate, DeliveryRequestID) VALUES ("
        l_strSQL = l_strSQL & "39, "
        l_strSQL = l_strSQL & "'" & GetData("events", "bigfilesize", "eventID", lp_lngEventID) & "', "
        l_strSQL = l_strSQL & "convert(nvarchar, getdate(), 20), "
        l_strSQL = l_strSQL & l_lngDeliveryRequestID & ");"
        Debug.Print l_strSQL
        cnn.Execute l_strSQL
        
        '2) Add each file to that Request
        l_strSQL = "INSERT INTO Files (RequestID, Filename, Path) VALUES ("
        l_strSQL = l_strSQL & l_lngDeliveryRequestID & ", "
        l_strSQL = l_strSQL & "'" & GetData("events", "clipfilename", "eventID", l_lngXMLclipID) & "', "
        l_strSQL = l_strSQL & "'" & GetData("library", "subtitle", "libraryID", l_lngDeliveryLibraryID) & "\" & GetData("events", "altlocation", "eventID", l_lngXMLclipID)
        l_strSQL = l_strSQL & "');"
        Debug.Print l_strSQL
        cnnTransfer.Execute l_strSQL
        l_strSQL = "INSERT INTO Files (RequestID, Filename, Path) VALUES ("
        l_strSQL = l_strSQL & l_lngDeliveryRequestID & ", "
        l_strSQL = l_strSQL & "'" & GetData("events", "clipfilename", "eventID", lp_lngEventID) & "', "
        l_strSQL = l_strSQL & "'" & GetData("library", "subtitle", "libraryID", l_lngDeliveryLibraryID) & "\" & GetData("events", "altlocation", "eventID", lp_lngEventID)
        l_strSQL = l_strSQL & "');"
        Debug.Print l_strSQL
        cnnTransfer.Execute l_strSQL
        
        '3) Add a recipient
        l_strSQL = "INSERT INTO Recipients (RequestID, Name, Email, [To], IsVDMS) VALUES ("
        l_strSQL = l_strSQL & l_lngDeliveryRequestID & ", "
        l_strSQL = l_strSQL & "'Tim', "
        l_strSQL = l_strSQL & "'thart@visualdatamedia.com', 1, 1"
        l_strSQL = l_strSQL & ");"
        Debug.Print l_strSQL
        cnnTransfer.Execute l_strSQL
        l_strSQL = "INSERT INTO Recipients (RequestID, Name, Email, [To], IsVDMS) VALUES ("
        l_strSQL = l_strSQL & l_lngDeliveryRequestID & ", "
        l_strSQL = l_strSQL & "'Christian', "
        l_strSQL = l_strSQL & "'cedwards@visualdatamedia.com', 1, 1"
        l_strSQL = l_strSQL & ");"
        Debug.Print l_strSQL
        cnnTransfer.Execute l_strSQL
        
        '4) Set the StatusID to 1, to kick things off.
        l_strSQL = "UPDATE Request SET StatusID = 1 WHERE RequestID = " & l_lngDeliveryRequestID
        Debug.Print l_strSQL
        cnnTransfer.Execute l_strSQL
        
        '5) Make logging entries for the deliveries.
        'Log the delivery of the XML
        l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, bigfilesize, accesstype, finalstatus) VALUES ("
        l_strSQL = l_strSQL & "2753, "
        l_strSQL = l_strSQL & "1261, "
        l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
        l_strSQL = l_strSQL & l_lngXMLclipID & ", "
        l_strSQL = l_strSQL & GetData("events", "bigfilesize", "eventID", l_lngXMLclipID) & ", "
        l_strSQL = l_strSQL & "'DADC Delivery', "
        l_strSQL = l_strSQL & "'Completed')"
        Debug.Print l_strSQL
        cnn.Execute l_strSQL
        'Log a Webdelivery for the item.
        l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, bigfilesize, accesstype, finalstatus) VALUES ("
        l_strSQL = l_strSQL & "2753, "
        l_strSQL = l_strSQL & "1261, "
        l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
        l_strSQL = l_strSQL & lp_lngEventID & ", "
        l_strSQL = l_strSQL & BigFileSize & ", "
        l_strSQL = l_strSQL & "'DADC Delivery', "
        l_strSQL = l_strSQL & "'Completed')"
        Debug.Print l_strSQL
        cnn.Execute l_strSQL
        
        l_strSQL = "UPDATE tracker_dadc_item SET stagefield4 = '" & FormatSQLDate(Now) & "' WHERE itemreference = '" & txtReference & "' AND readytobill = 0;"
        cnn.Execute l_strSQL
        l_strSQL = "UPDATE tracker_dadc_item SET xmlmadeby = '" & RequestName & "' WHERE itemreference = '" & txtReference & "' AND readytobill = 0;"
        cnn.Execute l_strSQL
        MakeDADCXMLFile = True
    Else
        MakeDADCXMLFile = True
    End If

Else
    
    If txtReference = "" Then
        g_strFailComment = "Essential information has not been completed - no Reference Entry."
    ElseIf txtTimecodestart = "" Then
        g_strFailComment = "Essential information has not been completed - no Timecode Start Entry."
    ElseIf txtTimecodestop = "" Then
        g_strFailComment = "Essential information has not been completed - no Timecode End Entry."
    ElseIf cmbFrameRate = "" Then
        g_strFailComment = "Essential information has not been completed - no Framerate Entry."
    ElseIf cmbIsTextInPicture = "" Then
        g_strFailComment = "Essential information has not been completed - no Text In Picture Entry."
    ElseIf Len(txtDuration) <> 11 Then
        g_strFailComment = "Essential information has not been completed - Duration field is not correctly entered."
    ElseIf adoLogging.RecordCount < 4 Then
        g_strFailComment = "Essential information has not been completed - Timecode Logging Information is not complete."
    ElseIf cmbAudioType(1) = "" Or cmbAudioContent(1) = "" Or cmbAudioLanguage(1) = "" Then
        g_strFailComment = "Essential information has not been completed - Audio Logging Information is not complete."
    End If

End If

Exit Function

DADC_XML_ERROR3:

g_strFailComment = "Error: " & Err.Number & ", " & Err.Description

ABORT_XML:
Close #1
Set FSO = New Scripting.FileSystemObject
FSO.DeleteFile l_strFileNameToSave
Exit Function

SENDING_ERROR:
g_strFailComment = "There was a problem doing the autosend - it has probably not happened."
Exit Function

MOVE_ERROR:
g_strFailComment = "There was a problem moving the asset. Is it still open somewhere?"
Exit Function

End Function

Private Sub WriteTabs(lp_intTabCount As Integer)

Dim Count As Integer

For Count = 1 To lp_intTabCount

    Print #1, Chr(9);

Next

End Sub

Sub WriteFileDescriptor(lp_blnVOD As Boolean, lp_intTabCount As Integer, Optional lp_strAudioType As String, Optional lp_strRequirements As String, Optional txtClipfilename As String, Optional txtReference As String, _
Optional txtVertPixels As String, Optional cmbAspect As String, Optional cmbFrameRate As String, Optional cmbInterlace As String, Optional txtColorSpace As String, Optional cmbClipformat As String, Optional cmbClipCodec As String)
 
WriteTabs (lp_intTabCount)
Print #1, "<inv:FileDescriptors xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
WriteTabs (lp_intTabCount + 1)
Print #1, "<inv:FileDescriptor>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:FileName>" & txtClipfilename & "</inv:FileName>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodingEquipment></inv:EncodingEquipment>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodingProfile>";
Select Case Val(txtVertPixels)
    Case 576
        If IsEmpty(lp_strAudioType) Or lp_strAudioType = "" Then
            If lp_blnVOD = True Then Print #1, "VOD - ";
            Print #1, "BBCW ProRes PAL ";
            If cmbAspect = "16:9" Then
                Print #1, "16x9";
            Else
                Print #1, "4x3";
            End If
        Else
            If lp_strAudioType = "Standard Stereo" Or lp_strAudioType = "Stereo" Then
                Print #1, "BBCW Muxed Stereo MOV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Muxed 5.1 MOV PCM";
            ElseIf lp_strAudioType = "Mono 2.0" Then
                Print #1, "BBCW Muxed Mono 2.0 MOV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Single Channel Muxed Mono MOV PCM";
            ElseIf Left(lp_strAudioType, 7) = "Dolby E" Then
                Print #1, "BBCW Muxed DolbyE MOV";
            End If
        End If
    Case 486
        If IsEmpty(lp_strAudioType) Or lp_strAudioType = "" Then
            If lp_blnVOD = True Then Print #1, "VOD - ";
            Print #1, "BBCW ProRes NTSC ";
            If cmbAspect = "16:9" Then
                Print #1, "16x9";
            Else
                Print #1, "4x3";
            End If
        Else
            If lp_strAudioType = "Standard Stereo" Or lp_strAudioType = "Stereo" Then
                Print #1, "BBCW Muxed Stereo MOV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Muxed 5.1 MOV PCM";
            ElseIf lp_strAudioType = "Mono 2.0" Then
                Print #1, "BBCW Muxed Mono 2.0 MOV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Single Channel Muxed Mono MOV PCM";
            ElseIf Left(lp_strAudioType, 7) = "Dolby E" Then
                Print #1, "BBCW Muxed DolbyE MOV";
            End If
        End If
    Case 1080
        If IsEmpty(lp_strAudioType) Or lp_strAudioType = "" Then
            If lp_blnVOD = True Then Print #1, "VOD - ";
            Print #1, "BBCW ProRes ";
            If InStr(lp_strRequirements, "3D Left") > 0 Then
                Print #1, "3D Left ";
            ElseIf InStr(lp_strRequirements, "3D Right") > 0 Then
                Print #1, "3D Right ";
            End If
            If cmbFrameRate = "23.98" Then
                Print #1, "1080 24p ";
            ElseIf cmbFrameRate = "29.97 NDF" And cmbInterlace = "Progressive" Then
                Print #1, "1080 29.97p ";
            ElseIf cmbFrameRate = "29.97" And cmbInterlace = "Progressive" Then
                Print #1, "1080 29.97p ";
            ElseIf cmbFrameRate = "29.97 NDF" And cmbInterlace = "Interlace" Then
                Print #1, "1080 59.94i ";
            ElseIf cmbFrameRate = "29.97" And cmbInterlace = "Interlace" Then
                Print #1, "1080 59.94i ";
            ElseIf cmbFrameRate = "25" And cmbInterlace = "Progressive" Then
                Print #1, "1080 25p ";
            ElseIf cmbFrameRate = "25" And cmbInterlace = "Interlace" Then
                Print #1, "1080 50i ";
            End If
            Print #1, "16x9";
        Else
            If lp_strAudioType = "Standard Stereo" Or lp_strAudioType = "Stereo" Then
                Print #1, "BBCW Muxed Stereo MOV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Muxed 5.1 MOV PCM";
            ElseIf lp_strAudioType = "Mono 2.0" Then
                Print #1, "BBCW Muxed Mono 2.0 MOV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Single Channel Muxed Mono MOV PCM";
            ElseIf Left(lp_strAudioType, 7) = "Dolby E" Then
                Print #1, "BBCW Muxed DolbyE MOV";
            End If
        End If
    Case 2160
        If IsEmpty(lp_strAudioType) Or lp_strAudioType = "" Then
            If lp_blnVOD = True Then Print #1, "VOD - ";
            If cmbClipCodec = "ProRes 4444" Then
                Print #1, "BBCW ProRes 444 UHD ";
            Else
                Print #1, "BBCW ProRes UHD ";
            End If
            If cmbFrameRate = "29.97" Then
                Print #1, "29.97p";
            ElseIf cmbFrameRate = "25" Then
                Print #1, "25p";
            End If
            If Right(txtColorSpace, 3) = "HDR" Then
                Print #1, " HDR";
            End If
        Else
            If lp_strAudioType = "Standard Stereo" Or lp_strAudioType = "Stereo" Then
                Print #1, "BBCW Muxed Stereo MOV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Muxed 5.1 MOV PCM";
            ElseIf lp_strAudioType = "Mono 2.0" Then
                Print #1, "BBCW Muxed Mono 2.0 MOV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Single Channel Muxed Mono MOV PCM";
            ElseIf Left(lp_strAudioType, 7) = "Dolby E" Then
                Print #1, "BBCW Muxed DolbyE MOV";
            End If
        End If
    Case Else
        'Audio only file...
        If cmbClipformat = "BWAV" Then
            If lp_strAudioType = "Standard Stereo" Or lp_strAudioType = "Stereo" Then
                Print #1, "BBCW Discrete Stereo BWAV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Discrete 5.1 BWAV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Discrete Single Channel Mono BWAV PCM";
            End If
        ElseIf Left(cmbClipformat, 3) = "RAR" Then
            If lp_strAudioType = "Standard Stereo" Or lp_strAudioType = "Stereo" Then
                Print #1, "BBCW Discrete Stereo BWAV RAR";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Discrete 5.1 BWAV RAR";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Discrete Mono BWAV RAR";
            End If
        Else
            If InStr(lp_strRequirements, "Multiple") > 0 Then
                If lp_strAudioType = "Standard Stereo" Or lp_strAudioType = "Stereo" Then
                    Print #1, "BBCW Discrete Multiple Tracks Stereo MOV PCM";
                ElseIf lp_strAudioType = "5.1" Then
                    Print #1, "BBCW Discrete Multiple Tracks 5.1 MOV PCM";
                ElseIf lp_strAudioType = "Mono 2.0" Then
                    Print #1, "BBCW Discrete Multiple Tracks Mono 2.0 MOV PCM";
                ElseIf lp_strAudioType = "Mono" Then
                    Print #1, "BBCW Discrete Multiple Tracks Single Channel Mono MOV PCM";
                End If
            Else
                If lp_strAudioType = "Standard Stereo" Or lp_strAudioType = "Stereo" Then
                    Print #1, "BBCW Discrete Track Stereo MOV PCM";
                ElseIf lp_strAudioType = "5.1" Then
                    Print #1, "BBCW Discrete Track 5.1 MOV PCM";
                ElseIf lp_strAudioType = "Mono 2.0" Then
                    Print #1, "BBCW Discrete Track Mono 2.0 MOV PCM";
                ElseIf lp_strAudioType = "Mono" Then
                    Print #1, "BBCW Discrete Track Single Channel Mono MOV PCM";
                End If
            End If
        End If
End Select
If IsEmpty(lp_strAudioType) Or lp_strAudioType = "" Then
    If InStr(cmbClipCodec, "UPRES") > 0 Then Print #1, " UPRES";
End If
Print #1, "</inv:EncodingProfile>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:ChecksumValue></inv:ChecksumValue>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:ChecksumType></inv:ChecksumType>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:FileFormat></inv:FileFormat>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodeDate>" & Format(GetData("tracker_dadc_item", "stagefield2", "itemreference", txtReference), "YYYY-MM-DDTHH:NN:SS") & "</inv:EncodeDate>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodeVendorOrganizationName>VDMS - London</inv:EncodeVendorOrganizationName>"
WriteTabs (lp_intTabCount + 1)
Print #1, "</inv:FileDescriptor>"
WriteTabs (lp_intTabCount)
Print #1, "</inv:FileDescriptors>"

End Sub

Function XMLSanitise(ByVal lp_strText As Variant) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strNewText As String
    Dim l_strOldText As String
    Dim l_intLoop As Integer
    l_strNewText = ""
    
    If IsNull(lp_strText) Then
        l_strOldText = ""
        l_strNewText = l_strOldText
        lp_strText = ""
    Else
        l_strOldText = lp_strText
    End If
    
    If InStr(lp_strText, "<") <> 0 Or InStr(lp_strText, ">") <> 0 Or InStr(lp_strText, "&") <> 0 Then
        
        For l_intLoop = 1 To Len(l_strOldText)
            
            Select Case Mid(l_strOldText, l_intLoop, 1)
            
                Case ">"
                    l_strNewText = l_strNewText & "&gt;"
                Case "<"
                    l_strNewText = l_strNewText & "&lt;"
                Case "&"
                    l_strNewText = l_strNewText & "&amp;"
                Case Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
            
            End Select
        
        Next
    Else
        l_strNewText = lp_strText
    End If
    
    If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
        l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
    End If
    
    XMLSanitise = l_strNewText
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub WriteAudioChannelComponents(lp_intTabCount As Integer, lp_strType As String, ByRef lp_lngChannelCount As Long, lp_blnVOD As Boolean, Optional lp_strAudioContent As String, Optional lp_strAudioType As String, _
Optional txtClipfilename As String, Optional txtReference As String, Optional txtVertPixels As String)

If lp_strType = "2track" Then
    
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType, "", txtClipfilename, txtReference, txtVertPixels
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
'    If lp_strAudioContent = "MOS" Or lp_strAudioType = "Mono 2.0" Or lp_strAudioType = "Dolby E" Then
    If Left(lp_strAudioType, 7) = "Dolby E" Then
        Print #1, "<inv:AudioChannelAssignment>Dolby E 1</inv:AudioChannelAssignment>"
    ElseIf lp_strAudioContent = "MOS" Then
        Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    Else
        Print #1, "<inv:AudioChannelAssignment>Left</inv:AudioChannelAssignment>"
    End If
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType, "", txtClipfilename, txtReference, txtVertPixels
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 2 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 2 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
'   If lp_strAudioContent = "MOS" Or lp_strAudioType = "Mono 2.0" Or lp_strAudioType = "Dolby E" Then Dolby E 1
    If Left(lp_strAudioType, 7) = "Dolby E" Then
        Print #1, "<inv:AudioChannelAssignment>Dolby E 2</inv:AudioChannelAssignment>"
    ElseIf lp_strAudioContent = "MOS" Then
        Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    Else
        Print #1, "<inv:AudioChannelAssignment>Right</inv:AudioChannelAssignment>"
    End If
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 2

ElseIf lp_strType = "1track" Then
    
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType, "", txtClipfilename, txtReference, txtVertPixels
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 1

ElseIf lp_strType = "6track" Then

    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType, "", txtClipfilename, txtReference, txtVertPixels
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Left</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType, "", txtClipfilename, txtReference, txtVertPixels
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 2 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 2 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Right</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType, "", txtClipfilename, txtReference, txtVertPixels
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 3 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 3 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Front Center</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType, "", txtClipfilename, txtReference, txtVertPixels
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 4 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 4 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>LFE</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType, "", txtClipfilename, txtReference, txtVertPixels
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 5 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 5 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Rear Left</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType, "", txtClipfilename, txtReference, txtVertPixels
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 6 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 6 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Rear Right</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 6

End If

End Sub

